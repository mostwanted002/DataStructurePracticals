//Double Ended Queue implementation.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>

using namespace std;

template <class T> 
class Node{
    T data;
    Node<T> *next;
    Node<T> *prev;
public:
    Node<T>(){
        data = 0;
        next = 0;
        prev = 0;
    }
    Node<T>(T data, Node<T> *n = NULL, Node<T> *p = NULL){
        this->data = data;
        next = n;
        prev = p;
    }
    T getData(){
        return data;
    }
    Node<T> *getNext(){
        return next;
    }
    Node<T> *getPrev(){
        return prev;
    }
    void setNext(Node<T> *next){
        this->next = next;
    }
    void setPrev(Node<T> *prev){
        this->prev = prev;
    }
    void setData(T data){
        this->data = data;
    }
};

template <class T> 
class Dequeue{
    Node<T> *head;
    Node<T> *tail;
    int count;
public:
    Dequeue(){
        head = 0;
        tail = 0;
        count = 0;
    }
    void insertAtHead(T data){
        if (count == 0){
            head = new Node<T>(data);
            tail = head;
            head->setNext(tail);
            tail->setNext(0);
            count++;
        }
        else{
        Node<T> *temp = head;
        head = new Node<T>(data, head);
        temp->setPrev(head);
        count++;
        }
    }
    void insertAtTail(T data){
        if (count == 0){
            tail = new Node<T>(data);
            head = tail;
            tail->setPrev(head);
            count++;
        }
        else{
        Node<T> *temp = tail;
        tail = new Node<T>(data, 0, tail);
        temp->setNext(tail);
        count++;
        }
    }
    void deleteAtHead(){
        if (count == 0){
            cout<<"EMPTY QUEUE! \n";
        }
        else if(count == 1){
            cout<<head->getData()<<endl;
            head = 0; tail = 0;
            count--;
        }
        else{
            cout<<head->getData()<<endl;
            head = head->getNext();
            head->setPrev(0);
            count--;
        }
    }
    void deleteAtTail(){
        if (count == 0){
            cout<<"EMPTY QUEUE! \n";
        }
        else if(count == 1){
            cout<<tail->getData()<<endl;
            head = 0; tail = 0;
            count--;
        }
        else{
            cout<<tail->getData()<<endl;
            tail = tail->getPrev();
            tail->setNext(0);
            count--;
        }
    }
    void display(){
        Node<T> *temp;
        if (count == 0){
            cout<<"EMPTY QUEUE! \n";
        }
        else{
        cout<<"| <-> ";
        for(temp=head; temp->getNext() != 0; temp = temp->getNext())
            cout<<temp->getData()<<" <-> ";
        cout<<temp->getData()<<" <-> ";
        cout<<"| \n";
        }
    }
};

int main(){
    int choice, d, node;
    system("clear");
    Dequeue <int> dll;
    do{        
        cout<<"(1) ENQUEUE at FRONT. \n";
        cout<<"(2) ENQUEUE at REAR. \n";
        cout<<"(3) DEQUEUE at FRONT. \n";
        cout<<"(4) DEQUEUE at REAR. \n";
        cout<<"(5) PRINT the QUEUE. \n";
        cout<<"(0) EXIT. \n";
        cout<<"Enter your choice no: ";
        cin>>choice;
        switch(choice)
        {
            case 1:{
                cout<<"Enter the data to enter: ";
                cin>>d;
                dll.insertAtHead(d);
            }
            break; 
            case 2:{
                cout<<"Enter the data to enter: ";
                cin>>d;
                dll.insertAtTail(d);
            }
            break;
            case 3:{
                dll.deleteAtHead();
            }
            break;
            case 4:{
                dll.deleteAtTail();
            }
            break;
            case 5:{
                dll.display();
            }
            break;
            case 0: break;
        }
    }while (choice != 0);
    return 0;
}
