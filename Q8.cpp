//Queue Implementation Using Circular Array
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdlib.h>

using namespace std;

template <class T>
class Queue{
    T *array;
    T temp;
    int size, rear, count;
    const int front = 0;
public:
    Queue<T> (){
        cout<<"Enter the size of Queue: ";
        cin>>this->size;
        array = new T [size];
        rear = -1;
        count = 0;
    }
    void enqueue(T data){
        if(count == size)
            cout<<"ERROR! QUEUE OVERFLOW \n";
        else{
            count++;
            shift();
            this->array[front] = data;
            rear++;
        }
    }
    void dequeue(){
        if(count == 0)
            cout<<"ERROR! QUEUE UNDERFLOW \n";
        else{
            cout<<"DEQUEUED: "<<this->array[rear]<<endl;
            this->array[rear] = 0;
            rear--;
            count--;
        }
    }
    void printQueue(){
        if(count == 0)
            cout<<"EMPTY QUEUE! \n";
        else if(count == 1)
            cout<<this->array[count-1]<<endl;
        else{
            for(int i = count-1; i >= 0; i--)
                cout<<this->array[i]<<" - ";
            cout<<endl;
        }
    }
    void shift(){
        for(int i = (count-1) ; i >= 0; i--)
            this->array[i+1] = this->array [i];
    }
};

int main(){
    int choice, data, size;
    system("clear");
    Queue <int> qobj;
    do{
        cout<<"(1) ENQUEUE \n";
        cout<<"(2) DEQUEUE \n";
        cout<<"(3) PRINT \n";
        cout<<"(0) EXIT \n";
        cout<<"Enter your choice: ";
        cin>>choice;
        switch(choice){
            case 1:{
                cout<<"Enter data to enqueue: ";
                cin>>data;
                qobj.enqueue(data);
            }
            break;
            case 2:{
                qobj.dequeue();
            }
            break;
            case 3:{
                qobj.printQueue();
            }
            break;
            case 0:{
                cout<<"GOODBYE! \n";
            }
            break;
            default:{
                cout<<"Error! Please select a valid option!! \n";
            }
            break;
        }
    }while(choice != 0);
    return 0;
}
