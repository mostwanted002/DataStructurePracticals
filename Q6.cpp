//Stack Implementation using Linked List
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdlib.h>

using namespace std;

template <class T> 
class Node{
    T data;
    Node<T> *next;
public:
    Node<T>(){
        data = 0;
        next = 0;
    }
    Node<T>(T data, Node<T> *n = NULL){
        this->data = data;
        next = n;
    }
    T getData(){
        return data;
    }
    Node<T> *getNext(){
        return next;
    }
    void setNext(Node<T> *next){
        this->next = next;
    }
    void setData(T data){
        this->data = data;
    }
};

template <class T> class Stack{
    Node<T> *head, *tail;
    int sizeOfStack, counter;
public:
    Stack<T>(){
        head = 0;
        tail = 0;
        sizeOfStack = 0;
        counter = 0;
    }
    Stack<T>(int size){
        head = 0;
        counter = 0;
        tail = 0;
        sizeOfStack = size;
    }
    void push(T data){
        if (counter == sizeOfStack)
            cout<<"ERROR STACK-OVERFLOW \n";
        else{
            if (sizeOfStack == 0){
                head = new Node<T>(data, tail);
                tail = head;
                tail->setNext(0);
            }
            else
                head = new Node<T>(data, head);
            counter++;
        }
    }
    T pop(){
        T data;
        if(counter == 0)
            return -1;
        else{
            data = head->getData();
            deleteAtHead();
            counter--;
        }
        return data;
    }
    void deleteAtHead(){
        Node<T> *temp;
        temp = head;
        head = head->getNext();
        delete temp;
    }
    void printStack(){
        if(counter == 0)
            cout<<"ERROR EMPTY STACK! \n";
        else{
        Node<T> *temp;
        cout<<"_ _ _ _ _\n";
        for(temp = head; temp->getNext() != 0; temp = temp->getNext())
            cout<<"|   "<<temp->getData()<<"   |\n";
        cout<<"|   "<<temp->getData()<<"   |\n";
        cout<<"` ` ` ` `\n";
        }
    }
};

int main(){
    int data, choice, s;
    cout<<"Enter the size of stack: ";
    cin>>s;
    Stack<int> newStack(s);
    system("clear");
    do{
        cout<<"(1) PUSH \n";
        cout<<"(2) POP \n";
        cout<<"(3) DISPLAY\n";
        cout<<"(0) EXIT\n";
        cout<<"Enter choice number: ";
        cin>>choice;
        switch(choice){
            case 1:{
                cout<<"Enter the data to be pushed: ";
                cin>>data;
                newStack.push(data);
            }
            break;
            case 2:{
                data = newStack.pop();
                if (data == -1)
                    cout<<"ERROR STACK UNDERFLOW! \n";
                else
                    cout<<"POPPED: "<<data<<"\n";
            }
            break;
            case 3:{
                newStack.printStack();
            }
            break;
            case 0:{
                cout<<"GOODBYE!! \n";
            }
            break;
        }
    }while(choice != 0);
}
