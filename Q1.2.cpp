//Linear Search implementation.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>

using namespace std;

template <class t>
    void linearSearch(t* array, t x, int size)
    {
        int flag = 0;
        int i =0;
        for (i = 0; i<size; i++)
            if (x==array[i]) cout<<"Element "<<x<<" EXISTS at index "<<i<<endl;
    }

int main()
{
    int* arrayOfInt;
    float* arrayOfFloat;
    char* arrayOfChar;
    int choice, size;
    cout<<"Select the type of array you wish to enter: "<<endl;
    cout<<"1. Integers"<<endl;
    cout<<"2. Float"<<endl;
    cout<<"3. Characters"<<endl;
    cin>>choice;
        
    switch(choice)
    {
        case 1:
        {
            
            cout<<"Enter the number of elements you wish to enter";
            cin>>size;
            arrayOfInt = new int [size];
            cout<<"Enter the elements- ";
            for (int i = 0; i<size; i++)
                cin>>arrayOfInt[i];
            int x;
            cout<<"Enter the element to search for: ";
            cin>>x;
            linearSearch(arrayOfInt, x, size);
            
        }
        break;
        case 2:
        {
            
            cout<<"Enter the number of elements you wish to enter: ";
            cin>>size;
            arrayOfFloat = new float [size];
            cout<<"Enter the elements- ";
            for (int i = 0; i<size; i++)
                cin>>arrayOfFloat[i];
            float x;
            cout<<"Enter the element to search for: ";
            cin>>x;
            linearSearch(arrayOfFloat, x, size);
            
        }
        break;
        default:
        {
            
            cout<<"Enter the number of elements you wish to enter";
            cin>>size;
            arrayOfChar = new char [size];
            cout<<"Enter the elements- ";
            for (int i = 0; i<size; i++)
                cin>>arrayOfChar[i];
            char x;
            cout<<"Enter the element to search for: ";
            cin>>x;
            linearSearch(arrayOfChar, x, size);
            
        }
        break;
    }
    return 0;
}
