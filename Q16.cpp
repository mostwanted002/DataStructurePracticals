//Stack Reversal using an Additional Stack.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdio.h>
using namespace std;

template <class t> class Stack
{
    int top,size;
    t* stack;
public:
    Stack<t>(){
        top = -1;
        size = 0;
        stack = 0;
    }
    Stack<t> copy(){
        Stack <t> copyobj;
        copyobj.top = top;
        copyobj.size = size;
        copyobj.stack = new t [copyobj.size];
        for(int i = 0; i < size; i++)
            copyobj.stack[i] = stack[i];
        return copyobj;
    }
    Stack<t>(int s)
    {
        top=-1;
        size = s;
        stack = new t [size];
        for (int i = 0; i < size; i++)
            stack[i] = '\0';
    }
    void push(t in)
    {
        if (top==(size-1))
            cout<<"STACK OVERFLOW!"<<endl;
        else
        {
            top++;
            stack[top] = in;
        }
    }
    t pop()
    {
        t temp = 0;
        if (top < 0)
            cout<<"STACK UNDERFLOW!"<<endl;
        else{
            temp = stack[top];
            stack[top] = '\0';
            top--;
        }
        return temp;
    }
    void display()
    {
        cout<<"Stack at present is: ";
        for(int i=0; i<size; i++)
            if(stack[i] != '\0') cout<<stack[i]<<" ";
        cout<<endl;
    }
    void reverse(){
        Stack copy1 = copy();
        int temp = top;
        for(int i = top; i>=0; i--)
            pop();
        for(int i = 0; i < size; i++)
            stack[i]=copy1.pop();
        top = temp;
    }
};

int main()
{
    system("clear");
    int s,choice,element;
    cout<<"Enter the size of stack: ";
    cin>>s;
    Stack <int> obj(s);
    do
    {
        obj.display();
        cout<<"Choose option you wish to perform: "<<endl;
        cout<<"(1) PUSH an element."<<endl;
        cout<<"(2) POP the element."<<endl;
        cout<<"(3) REVERSE the stack."<<endl;
        cout<<"(4) EXIT."<<endl;
        cin>>choice;
        switch(choice)
        {
            case 1:
            {
                cout<<"Enter the element: ";
                cin>>element;
                obj.push(element);
            }
            break;
            case 2:
            {
                    cout<<obj.pop()<<endl;
            }
            break;
            case 3:
            {
                obj.reverse();
            }
        }
    }while(choice != 4);
    return 0;
}
