//Stack Implementation using Array.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdio.h>
using namespace std;

template <class t> class Stack
{
    int top,size;
    int* stack;
public:
    Stack()
    {}
    Stack(int s)
    {
        top=-1;
        size = s;
        stack = new t [size];
        for (int i = 0; i < size; i++)
            stack[i] = '\0';
    }
    void push(t in)
    {
        if (top==(size-1))
            cout<<"STACK OVERFLOW!"<<endl;
        else
        {
            top++;
            stack[top] = in;
        }
    }
    void pop()
    {
        if (top < 0)
            cout<<"STACK UNDERFLOW!"<<endl;
        else{
            stack[top] = '\0' ;
            top--;
        }
    }
    void display()
    {
        cout<<"Stack at present is: ";
        for(int i=0; i<size; i++)
            if(stack[i] != '\0') cout<<stack[i]<<" ";
        cout<<endl;
    }
};

int main()
{
    system("clear");
    int s,choice,element;
    cout<<"Enter the size of stack: ";
    cin>>s;
    Stack <int> obj(s);
    do
    {
        obj.display();
        cout<<"Choose option you wish to perform: "<<endl;
        cout<<"(1) PUSH an element."<<endl;
        cout<<"(2) POP the element."<<endl;
        cout<<"(3) EXIT."<<endl;
        cin>>choice;
        switch(choice)
        {
            case 1:
            {
                cout<<"Enter the element: ";
                cin>>element;
                obj.push(element);
            }
            break;
            case 2:
            {
                obj.pop();
            }
            break;
        }
    }while(choice != 3);
    return 0;
}
