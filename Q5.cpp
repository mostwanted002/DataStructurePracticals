//Circular Linked List implementation.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>

using namespace std;

    
//class for Node
template <class T> class Node{
    T data;
    Node *next;
public:
    //Constructors
    Node(){
        data = 0;
        next = 0;
    }
    Node(T in, Node *next= 0){
        this->data = in;
        this->next = next;
    }
    
    Node *getNext(){
        return this->next;
    }
    
    void setNext(Node *next){
        this->next = next;
    }
    
    T getData(){
        return data;
    }
    void setData(T data){
        this->data = data;
    }
};

//class for LinkedList
template <class T> class LinkedList{
private:
    Node <T>  *head;
    Node <T> *tail;
    int size;
public:
    //Constructor
    LinkedList(){
        head = 0;
        tail = 0;
        size = 0;
    }
    //Destrcutor
    ~LinkedList(){
        Node<T> *temp = head;
        while (head != 0)
        {
            head = head->getNext();
            delete temp;
            temp = head;
        }
    }
    
    //Operations
    void pushAtHead(T data){
        if (size == 0)
        {
            head = tail = new Node<T>(data);
            tail->setNext(head);
        }
        else
        {
            head = new Node<T>(data, head);
            tail->setNext(head);
        }
        size++;
    }

    void pushAtTail(T data){
        if (size == 0)
        {
            head = tail = new Node<T>(data);
        }
        else
        {
            Node<T> *temp = new Node<T>(data,head);
            tail->setNext(temp);
            tail = temp;
        }
        size++;
    }
    void pushAt(T data, T node){
        Node<T> *temp;
        Node<T> *newNode;
        if (head->getData() == data){
            pushAtHead(data);
            size++;
        }
        else if (tail->getData() == data){
            pushAtTail(data);
            size++;
        }
        else{
            
            for(temp = head; temp->getNext()->getData() != node && temp->getNext() != head; temp=temp->getNext())
                ;
            if (temp == 0)
                cout<<"ERROR NODE NOT FOUND! \n";
            else{
                newNode = new Node<T>(data,temp->getNext());
                temp->setNext(newNode);
                size++;
            }
        }
    }

    T popFromHead(){
        if (head != 0)
        {
            T data = head->getData();
            Node<T> *temp = head;
            if (head == tail)
            {
                head = tail = 0;
            }
            else
            {
                head = head->getNext();
            }
            delete temp;
            return data;
        }
        size--;
    }

    T popFromTail(){
        if (tail != 0)
        {
            T data = tail->getData();
            if (head == tail)
            {
                delete head;
                head = tail = 0;
            }
            else
            {
                Node<T> *temp;
                for (temp = head; temp->getNext() != tail; temp = temp->getNext())
                    ;
                delete tail;
                tail = temp;
                tail->setNext(head);
            }
            return data;
        }
        size--;
    }

    void deleteNode(T data){
        if (head == tail)
        {
            delete head;
            head = tail = 0;
            size= 0;
        }
        else if (head->getData() == data)
        {
            Node<T> *temp = head;
            head = head->getNext();
            delete temp;
            size--;
        }
        else if (tail->getData() == data)
        {
            Node<T> *temp;
            for (temp = head; temp->getNext() != tail; temp = temp->getNext())
                ;
            delete tail;
            tail = temp;
            tail->setNext(head);
            size--;
        }
        else
        {
            Node<T> *temp;
            for (temp = head->getNext(); temp->getNext()->getData() != data && temp != tail; temp = temp->getNext())
                ;
            Node<T> *prev = temp;
            temp = temp->getNext();
            prev->setNext(temp->getNext());
            delete temp;
            size--;
        }
    }
    void reverse(){
        int i;
        Node<T> *temp;
        Node<T> *temp1;
        Node<T> *temp2;
        if (size == 0)
            cout<<"EMPTY LIST!! \n";
        else if(size == 1){
            head = temp;
            head = tail;
            tail = temp;
        }
        else{
            T *tempArray = new T [size];
            for(temp = head, i = 0; i < size; i++, temp = temp->getNext()){
                tempArray[i] = temp->getData();
            }
            tempArray = getReversed(tempArray);
            for(i = 0, temp = head; i<size; i++, temp = temp->getNext()){
                temp->setData(tempArray[i]);
            }
        }
    }
    T *getReversed(T *array){
        T temp;
        int i,j;
        for(i = 0, j = (size-1); i < (size)/2; i++, j--){
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    
    int search(T data){
        int i;
        if(size == 0)
            cout<<"ERROR EMPTY LIST! \n";
        else if(head->getData() == data)
            return 1;
        else if(tail->getData() == data)
            return size;
        else{
            Node<T> *temp;
            for(i = 1,temp = head; temp->getNext()!=head; temp = temp->getNext(), i++){
                if(temp->getData() == data)
                return i;
                
            }
        }
        return -1;
    }
    void show(){
        Node<T> *temp;
        if (size == 0){
            cout<<"EMPTY LIST! \n";
        }
        else{
            cout<<"-=>";
        for(temp=head; temp->getNext() != head; temp = temp->getNext())
            cout<<temp->getData()<<" -> ";
        cout<<temp->getData()<<" -> ";
        cout<<"--^ \n";
        }
    }
};


int main()
{
    system("clear");
    int choice, data, position;
    LinkedList <int> obj;
    do{
        cout<<"(1) Push at Head \n"
            <<"(2) Push at Tail \n"
            <<"(3) Push at Specific Location \n"
            <<"(4) Pop from Head \n"
            <<"(5) Pop from Tail \n"
            <<"(6) Pop from Specific Location \n"
            <<"(7) Reverse List \n"
            <<"(8) Search Element \n"
            <<"(9) Print List \n"
            <<"(0) Exit \n"
            <<"Enter the operation you wish to perform: ";
        cin>>choice;
        switch(choice){
            case 1:{
                cout<<"Enter the data you wish to push: ";
                cin>>data;
                obj.pushAtHead(data);
            }
            break;
            case 2:{
                cout<<"Enter the data you wish to push: ";
                cin>>data;
                obj.pushAtTail(data);
            }
            break;
            case 3:{
                cout<<"The position at which you wish to push data: ";
                cin>>position;
                cout<<"Enter the data you wish to push: ";
                cin>>data;
                obj.pushAt(data, position);
            }
            break;
            case 4:{
                cout<<obj.popFromHead()<<"\n";
            }
            break;
            case 5:{
                cout<<obj.popFromTail()<<"\n";
            }
            break;
            case 6:{
                cout<<"Enter the data you wish to pop: ";
                cin>>data;
                obj.deleteNode(data);
            }
            break;
            case 7:{
                obj.reverse();
            }
            break;
            case 8:{
                cout<<"Enter the data you wish to search: ";
                cin>>data;
                if(obj.search(data)==-1)
                    cout<<"NODE NOT PRESENT!! \n";
                else
                    cout<<"The node is present at index "<<obj.search(data)<<". \n";
            }
            break;
            case 9:{
                obj.show();
            }
            break;
            case 0:{
                cout<<"GOODBYE!\n";
            }
            break;
            default:{
                cout<<"INVALID CHOICE!! TRY AGAIN."<<endl;
            }
        }
    }while(choice!=0);
    return 0;
}
