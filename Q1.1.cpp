//Binary Search.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>

using namespace std;

template <class t>
int binarySearch(t* array, t x, int l, int r)
{
    if (r>=l)
    {
        int mid = l + (r-l)/2;
        if (array[mid] == x) return mid;
        else if (x<array[mid]) return binarySearch(array, x, l, mid);
        else return binarySearch(array,x, mid, r);
    }
    return -1;
}

int main()
{
    int* arrayOfInt;
    float* arrayOfFloat;
    char* arrayOfChar;
    int choice, size;
    cout<<"Select the type of array you wish to enter: "<<endl;
    cout<<"1. Integers"<<endl;
    cout<<"2. Float"<<endl;
    cout<<"3. Characters"<<endl;
    cin>>choice;
        
    switch(choice)
    {
        case 1:
        {
            
            cout<<"Enter the number of elements you wish to enter";
            cin>>size;
            arrayOfInt = new int [size];
            cout<<"Enter the elements in asscending order- ";
            for (int i = 0; i<size; i++)
                cin>>arrayOfInt[i];
            int x;
            cout<<"Enter the element to search for: ";
            cin>>x;
            int index = binarySearch(arrayOfInt, x, 0, size-1);
            if(index == -1)
                cout<<"Element DOESN'T EXIST."<<endl;
            else cout<<"Element EXISTS at index "<<index<<endl;
            
        }
        break;
        case 2:
        {
            
            cout<<"Enter the number of elements you wish to enter: ";
            cin>>size;
            arrayOfFloat = new float [size];
            cout<<"Enter the elements in ascending order- ";
            for (int i = 0; i<size; i++)
                cin>>arrayOfFloat[i];
            float x;
            cout<<"Enter the element to search for: ";
            cin>>x;
            int index = binarySearch(arrayOfFloat, x, 0, size-1);
            if(index == -1)
                cout<<"Element DOESN'T EXIST."<<endl;
            else cout<<"Element EXISTS at index "<<index<<endl;
            
        }
        break;
        default:
        {
            
            cout<<"Enter the number of elements you wish to enter";
            cin>>size;
            arrayOfChar = new char [size];
            cout<<"Enter the elements in ascending order- ";
            for (int i = 0; i<size; i++)
                cin>>arrayOfChar[i];
            char x;
            cout<<"Enter the element to search for: ";
            cin>>x;
            int index = binarySearch(arrayOfChar, x, 0, size-1);
            if(index == -1)
                cout<<"Element DOESN'T EXIST."<<endl;
            else cout<<"Element EXISTS at index "<<index<<endl;
            
        }
        break;
    }
    return 0;
}

    
