//WAP to Scan Polynomials and Add them using Linked List.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>

using namespace std;


class Node{
    Node *next;
    int degree;
    float coff;
public:
    Node(){
        degree = 0;
        coff = 0;
        next = 0;
    }
    Node(int degree, float coff, Node *next = 0){
        this->degree = degree;
        this->coff = coff;
        this->next = next;
    }
    int getDegree(){
        return this->degree;
    }
    float getCoff(){
        return this->coff;
    }
    Node *getNext(){
        return this->next;
    }
};

class polynomial{
    int highestDegree;
    float *coffs;
    Node *head, *tail;
public:
    polynomial(){}
    polynomial(int highestDegree){
        this->highestDegree = highestDegree+1;
        head = 0;
        tail = 0;
        coffs = new float [this->highestDegree];
    }
    void passCoffs(float *coffs){
        for (int i = 0 ; i < highestDegree; i++){
            this->coffs[i] = coffs[i];
            pushAtHead(i, coffs[i]);
        }
    }
    void pushAtHead(int degree, float coff){
        if (head == 0)
        {
            head = tail = new Node(degree, coff);
        }
        else
        {
            head = new Node(degree, coff, head);
        }
    }
    polynomial operator+(const polynomial &p2){
        polynomial answer;
        if(this->highestDegree > p2.highestDegree)
            answer = polynomial(this->highestDegree-1);
        else
            answer = polynomial(p2.highestDegree-1);
        float *c1 = this->coffs;
        float *c2 = p2.coffs;
        float *c3 = new float [answer.highestDegree];
        for(int i = 0; i < answer.getDegree(); i++)
            c3[i] = c1[i] + c2[i];
        answer.passCoffs(c3);
        return answer;
    }
    int getDegree(){
        return this->highestDegree;
    }
    void print(){
        Node *temp;
        for(temp=head; temp->getNext() != 0; temp = temp->getNext()){
            if(temp->getCoff() != 0)
                cout<<"("<<temp->getCoff()<<"x^"<<temp->getDegree()<<") + ";
        }
        cout<<temp->getCoff()<<endl;
    }
};

int main(){
    system("clear");
    int degree1, degree2;
    cout<<"Enter the degree for polynomial 1: ";
    cin>>degree1;
    polynomial p1(degree1);
    float *c1 = new float[degree1 + 1];
    for(int i = 0; i <= degree1; i++){
        cout<<"Enter cofficient for degree "<<i<<": ";
        cin>>c1[i];
    }
    p1.passCoffs(c1);
    cout<<"Enter the degree for polynomial 2: ";
    cin>>degree2;
    polynomial p2(degree2);
    float *c2 = new float[degree2 + 1];
    for(int i = 0; i <= degree2; i++){
        cout<<"Enter cofficient for degree "<<i<<": ";
        cin>>c2[i];
    }
    p2.passCoffs(c2);
    cout<<"Polynomial 1: ";
    p1.print();
    cout<<"Polynomial 2: ";
    p2.print();
    polynomial p3;
    p3 = p1 + p2;
    cout<<"Sum of P1 and P2 is: ";
    p3.print();
    cin.get();
    return 0;
}
