//Doubly Linked List implementation.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>

using namespace std;

template <class T> 
class Node{
    T data;
    Node<T> *next;
    Node<T> *prev;
public:
    Node<T>(){
        data = 0;
        next = 0;
        prev = 0;
    }
    Node<T>(T data, Node<T> *n = NULL, Node<T> *p = NULL){
        this->data = data;
        next = n;
        prev = p;
    }
    T getData(){
        return data;
    }
    Node<T> *getNext(){
        return next;
    }
    Node<T> *getPrev(){
        return prev;
    }
    void setNext(Node<T> *next){
        this->next = next;
    }
    void setPrev(Node<T> *prev){
        this->prev = prev;
    }
    void setData(T data){
        this->data = data;
    }
};

template <class T> 
class doublyLinkedList{
    Node<T> *head;
    Node<T> *tail;
    int count;
public:
    doublyLinkedList(){
        head = 0;
        tail = 0;
        count = 0;
    }
    void insertAtHead(T data){
        if (count == 0){
            head = new Node<T>(data);
            tail = head;
            head->setNext(tail);
            count++;
        }
        else{
        Node<T> *temp = head;
        head = new Node<T>(data, head);
        temp->setPrev(head);
        count++;
        }
    }
    void insertAtTail(T data){
        if (count == 0){
            tail = new Node<T>(data);
            head = tail;
            tail->setPrev(head);
            count++;
        }
        else{
        Node<T> *temp = tail;
        tail = new Node<T>(data, 0, tail);
        temp->setNext(tail);
        count++;
        }
    }
    void insertAt(T data, T node){
        Node<T> *temp;
        Node<T> *newNode;
        if (head->getData() == data)
            insertAtHead(data);
        else if (tail->getData() == data)
            insertAtTail(data);
        else{
            
            for(temp = head; temp->getNext()->getData() != node && temp->getNext() != 0; temp=temp->getNext())
                ;
            if (temp == 0)
                cout<<"ERROR NODE NOT FOUND! \n";
            else{
                newNode = new Node<T>(data,temp->getNext(),temp);
                temp->getNext()->setPrev(newNode);
                temp->setNext(newNode);
            }
        }
        count++;
    }
    void deleteAtHead(){
        if (count == 0){
            cout<<"EMPTY LIST! \n";
        }
        else if(count == 1){
            head = 0; tail = 0;
            count--;
        }
        else{
        head = head->getNext();
        head->setPrev(0);
        count--;
        }
    }
    void deleteAtTail(){
        if (count == 0){
            cout<<"EMPTY LIST! \n";
        }
        else if(count == 1){
            head = 0; tail = 0;
            count--;
        }
        else{
        tail = tail->getPrev();
        tail->setNext(0);
        count--;
        }
    }
    void deleteAt(T data){
        if (count == 0){
            cout<<"EMPTY LIST! \n";
            count--;
        }
        else{
            int i;
            Node<T> *temp;
            for(temp = head, i = 1; temp->getData() != data && temp->getNext() != 0 && i < count; temp = temp->getNext(), i++)
                ;
            if (temp->getNext() == 0)
                cout<<"ERROR NODE NOT FOUND \n";
            else{
                temp->getNext()->setPrev(temp->getPrev());
                temp->getPrev()->setNext(temp->getNext());
                delete temp;
            }
            count--;
        }
    }
    int search(T data){
        int i;
        if(count == 0)
            cout<<"ERROR EMPTY LIST! \n";
        else if(head->getData() == data)
            return 1;
        else if(tail->getData() == data)
            return count;
        else{
            Node<T> *temp;
            for(i = 1,temp = head; temp->getNext()!=0; temp = temp->getNext(), i++){
                if(temp->getData() == data)
                return i;
                
            }
        }
        return -1;
    }
    void reverse(){
        int i;
        Node<T> *temp;
        Node<T> *temp1;
        Node<T> *temp2;
        if (count == 0)
            cout<<"EMPTY LIST!! \n";
        else if(count == 1){
            head = temp;
            head = tail;
            tail = temp;
        }
        else{
            temp1 = head;
            temp2 = tail;
            for(i = 1, temp1 = head, temp2 = tail; i <= (count+1)/2; i++, temp1 = temp1->getNext(), temp2 = temp2->getPrev()){
                swap(temp1,temp2);
            }
        }
    }
    void swap(Node<T> *temp1,Node<T> *temp2){
        T temp = temp1->getData();
        temp1->setData(temp2->getData());
        temp2->setData(temp);
    }
    void display(){
        Node<T> *temp;
        if (count == 0){
            cout<<"EMPTY LIST! \n";
        }
        else{
        cout<<"NULL <-> ";
        for(temp=head; temp->getNext() != 0; temp = temp->getNext())
            cout<<temp->getData()<<" <-> ";
        cout<<temp->getData()<<" <-> ";
        cout<<"NULL \n";
        }
    }
};

int main(){
    int choice, d, node;
    system("clear");
    doublyLinkedList <int> dll;
    do{        
        cout<<"(1) Insert at HEAD. \n";
        cout<<"(2) Insert at SPECIFIC NODE. \n";
        cout<<"(3) Insert at TAIL. \n";
        cout<<"(4) Delete at HEAD. \n";
        cout<<"(5) Delete at SPECIFIC NODE. \n";
        cout<<"(6) Delete at TAIL. \n";
        cout<<"(7) Search the list. \n";
        cout<<"(8) Reverse the list. \n";
        cout<<"(9) Print the list. \n";
        cout<<"(0) EXIT. \n";
        cout<<"Enter your choice no: ";
        cin>>choice;
        switch(choice)
        {
            case 1:{
                cout<<"Enter the data to enter: ";
                cin>>d;
                dll.insertAtHead(d);
            }
            break;
            case 2:{
                cout<<"Enter the data to enter: ";
                cin>>d;
                cout<<"Enter the node prior to which the node is to be inserted: ";
                cin>>node;
                dll.insertAt(d,node);
            }
            break; 
            case 3:{
                cout<<"Enter the data to enter: ";
                cin>>d;
                dll.insertAtTail(d);
            }
            break;
            case 4:{
                dll.deleteAtHead();
            }
            break;
            case 5:{
                cout<<"Enter the node to be deleted: ";
                cin>>d;
                dll.deleteAt(d);
            }
            break;
            case 6:{
                dll.deleteAtTail();
            }
            break;
            case 7:{
                cout<<"Enter the node you wish to search for: ";
                cin>>d;
                if (dll.search(d) == -1)
                    cout<<"NODE NOT FOUND! \n";
                else
                    cout<<"Node is present at number "<<dll.search(d)<<". \n";
            }
            break;
            case 8:{
                dll.reverse();
            }
            break;
            case 9:{
                dll.display();
            }
            break;
            case 0: break;
        }
    }while (choice != 0);
    return 0;
}
