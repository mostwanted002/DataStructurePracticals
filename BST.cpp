//Binary Search Tree Implementation.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>

using namespace std;

class Node{
    int data;
    Node *left;
    Node *right;
    public:
    Node(){
        data = 0;
        left = 0;
        right = 0;
    }
    Node(int data, Node* n = 0){
        this->data = data;
        this->left = n; 
        this->right = n;
    }
    int getData(){
        return data;
    }
    Node *getNext(int data){
        if (this->data > data)
            return this->left;
        else
            return this->right;
    }
    Node *getLeft(){
        return this->left;
    }
    Node *getRight(){
        return this->right;
    }
    void setLeft(Node *left){
        this->left = left;
    }
    void setRight(Node *right){
        this->right = right;
    }
};

class BST{
    Node* root;
    int count;
    public:
    BST(){
        root = 0;
        count = 0;
    }
    BST(int data){
        root = new Node(data);
        count++;
    }
    void insert(int data){
        if (root == 0)
            root = new Node(data);
        else
            insert(root, data);
    }void insert(Node *node, int data){
        if (data < node->getData())
        {
            if (node->getLeft() == 0)
                node->setLeft(new Node(data));
            else
                insert(node->getLeft(), data);
        }
        else
        {
            if (node->getRight() == 0)
                node->setRight(new Node(data));
            else
                insert(node->getRight(), data);
        }
    void display(Node* parent){
        Node *temp = parent;
        if (temp == 0){
            return;
        }
        else{
            cout<<temp->getData()<<endl;
            cout<<"/    \\"<<endl;
            display(temp->getLeft());
            cout<<"       ";
            display(temp->getRight());
        }
        cout<<endl;
    }
    Node *getRoot(){
        return this->root;
    }
    int getCount(){
        return this->count;
    }
};

int main(){
    int d, choice;
    system("clear");
    BST newBST;
    do{
        cout<<"Select the operation: \n";
        cout<<"(1) Insert"<<endl;
        cout<<"(2) Delete"<<endl;
        cout<<"(3) Print"<<endl;
        cout<<"(4) Search"<<endl;
        cout<<"(0) Exit"<<endl;
        cin>>choice;
        switch(choice){
            case 1:{
                cout<<"Enter the data to input: ";
                cin>>d;
                newBST.insert(d);
                newBST.display(newBST.getRoot());
            }
            break;
            case 3:
                newBST.display(newBST.getRoot());
            break;
            case 0: cout<<"Goodbye! \n";
        }
    }while(choice != 0);
    return 0;
}
