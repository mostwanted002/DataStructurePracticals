//Program to convert Sparse Matrix to Non-Zero and Vice Versa.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdio.h>

using namespace std;

class Matrix
{
    int** sparseMatrix;
    int** nonZeroMatrix;
    int m,n;
    char type;
    public:
    Matrix()
    {
    }
    
    Matrix(int rows, int column, char t)
    {
        sparseMatrix = new int* [rows];
        nonZeroMatrix = new int* [rows*column];
        for(int i = 0;i<rows; i++)
            sparseMatrix[i] = new int [column];
        for(int i = 0;i<rows*column; i++)
            nonZeroMatrix[i] = new int [3];
        m = rows; n = column;
        
        type = t;
    }
    
    void input()
    {
        if (type == 's')
        {
            cout<<"INPUTTING SPARSE MATRIX: "<<endl;
            for (int i = 0; i<m; i++)
            {
                cout<<"Enter Row "<<(i+1)<<":"<<endl;
                for (int j = 0; j<n; j++)
                {
                    cout<<"Enter column "<<j+1<<": ";
                    cin>>sparseMatrix[i][j];
                }
            }
        }
        else
        {
            cout<<"INPUTING NON-ZERO MATRIX: "<<endl;
            int inp;
            for (int i = 0; i<m*n; i++)
            {
                cout<<"Enter Row "<<(i+1)<<":"<<endl;
                for (int j = 0; j<3; j++)
                {
                    
                    cout<<"Enter column "<<j+1<<"(or '0' to end input): ";
                    cin>>inp;
                    if(inp == 0) break;
                    nonZeroMatrix[i][j]= inp;
                }
                if (inp == 0) break;
            }
        }
    }
    
    void conversion()
    {
        if (type == 's')
        {
            int rowCount = 0;
            for(int i = 0; i < m; i++)
                for (int j = 0; j<n; j++)
                    if (sparseMatrix[i][j] != 0)
                    {
                        nonZeroMatrix[rowCount][0] = i+1;
                        nonZeroMatrix[rowCount][1] = j+1;
                        nonZeroMatrix[rowCount][2] = sparseMatrix[i][j];
                        rowCount++;
                    }
        }
        else
        {
            for (int i = 0; i < m*n; i++)
                if (nonZeroMatrix[i][2] != 0)
                    sparseMatrix[nonZeroMatrix[i][0]-1][nonZeroMatrix[i][1]-1] = nonZeroMatrix[i][2];
        }
    }
    
   void display()
    {
        if (type == 's')
        {
            cout<<"The Non-Zero Matrix for the provided Sparse Matrix is :"<<endl;
            for (int i = 0; i < m*n; i++)
            {
                for (int j = 0; j < 3; j++)
                    if (nonZeroMatrix[i][j] != '\0') cout<<nonZeroMatrix[i][j]<<" ";
                cout<<endl;
            }
        }
        else
        {
            cout<<"The Sparse Matrix for the provided Non-Zero Matrix is :"<<endl;
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j<n; j++)
                    cout<<sparseMatrix[i][j]<<" ";
                cout<<endl;
            }
        }
    }
    ~Matrix()
    {
    }
};
int main()
{
    system("clear");
    int menu, rows, column;
    do
    {
        cout<<"Type of Conversion you wish to perform:"<<endl; 
        cout<<"(1) SPARSE MATRIX to NON-ZERO MATRIX."<<endl;
        cout<<"(2) NON-ZERO MATRIX to SPARSE MATRIX."<<endl;
        cout<<"(3) EXIT."<<endl;
        cin>>menu;
        switch(menu)
        {
            case 1:
            {
                cout<<"Enter the number of rows: ";
                cin>>rows;
                cout<<"Enter the number of columns: ";
                cin>>column;
                Matrix obj(rows, column, 's');
                obj.input();
                obj.conversion();
                obj.display();
                cout<<endl;
                obj.~Matrix();
            }
            break;
            case 2:
            {
                cout<<"Enter the Maximum value in ""ROW"" COLUMN: ";
                cin>>rows;
                cout<<"Enter the Maximum value in ""COLUMN"" COLUMN: ";
                cin>>column;
                Matrix obj(rows, column, 'n');
                obj.input();
                obj.conversion();
                obj.display();
                cout<<endl;
                obj.~Matrix();
            }
            break;
        }
    }while(menu != 3);
    return 0;
}
