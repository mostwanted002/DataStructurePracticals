//Reversing a Stack using Additional Queue
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdlib.h>

using namespace std;

class Queue{
    int front, rear, size;
    int* queue;
public:
    Queue();
    Queue(int size){
        this->size = size;
        front = 0;
        rear = 0;
        queue = new int [size];
    }
    void enqueue(int data){
        if(rear == size){
            cout<<"QUEUE OVERFLOW!"<<endl;
        }
        else{
            queue[rear] = data;
            rear++;
        }
    }
    int dequeue(){
        int temp;
        if(queue[front] == '\0'){
            cout<<"QUEUE UNDERFLOW!"<<endl;
            temp = 0;
        }
        else{
            temp = queue[front];
            shift();
            rear--;
        }
        return temp;
    }
    void shift(){
        for(int i = 0; i <= rear-1; i++)
            this->queue[i] = this->queue[i+1];
    }
};

template <class t> class Stack
{
    int top,size;
    t* stack;
public:
    Stack<t>(){
        top = -1;
        size = 0;
        stack = 0;
    }
    Stack<t> copy(){
        Stack <t> copyobj;
        copyobj.top = top;
        copyobj.size = size;
        copyobj.stack = new t [copyobj.size];
        for(int i = 0; i < size; i++)
            copyobj.stack[i] = stack[i];
        return copyobj;
    }
    Stack<t>(int s)
    {
        top=-1;
        size = s;
        stack = new t [size];
        for (int i = 0; i < size; i++)
            stack[i] = '\0';
    }
    void push(t in)
    {
        if (top==(size-1))
            cout<<"STACK OVERFLOW!"<<endl;
        else
        {
            top++;
            stack[top] = in;
        }
    }
    t pop()
    {
        t temp = 0;
        if (top < 0)
            cout<<"STACK UNDERFLOW!"<<endl;
        else{
            temp = stack[top];
            stack[top] = '\0';
            top--;
        }
        return temp;
    }
    void display()
    {
        cout<<"Stack at present is: ";
        for(int i=0; i<size; i++)
            if(stack[i] != '\0') cout<<stack[i]<<" ";
        cout<<endl;
    }
    void reverse(){
        int temp = top;
        Queue copy(size);
        for(int i = 0; i < size; i++)
            copy.enqueue(pop());
        for(int i = 0; i < size; i++)
            this->push(copy.dequeue());
        top = temp;
    }
};

int main()
{
    system("clear");
    int s,choice,element;
    cout<<"Enter the size of stack: ";
    cin>>s;
    Stack <int> obj(s);
    do
    {
        obj.display();
        cout<<"Choose option you wish to perform: "<<endl;
        cout<<"(1) PUSH an element."<<endl;
        cout<<"(2) POP the element."<<endl;
        cout<<"(3) REVERSE the stack."<<endl;
        cout<<"(4) EXIT."<<endl;
        cin>>choice;
        switch(choice)
        {
            case 1:
            {
                cout<<"Enter the element: ";
                cin>>element;
                obj.push(element);
            }
            break;
            case 2:
            {
                    cout<<obj.pop()<<endl;
            }
            break;
            case 3:
            {
                obj.reverse();
            }
        }
    }while(choice != 4);
    return 0;
}
