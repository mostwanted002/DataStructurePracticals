//Converting a Matrix to Array
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi

#include<iostream>
#include<stdio.h>
using namespace std;

//Class for Matrix operations
class Matrix{
    //Data-Members
    int** sparseMatrix;
    int* array;
    int row, column, numOfElements;
    char type;
public:
    //Constructors
    Matrix(){
        row = 0;
        column = 0;
    }
    Matrix(int m, int n, char t){
        row = m;
        column = n;
        type = t;
        sparseMatrix = new int* [row];
        for(int i = 0; i<row; i++)
            sparseMatrix[i] = new int [column];
        if(type == 'l' || type == 'u'){
            numOfElements = (m*(m+1))/2;
            array = new int [numOfElements];
        }
        else{
            numOfElements = row;
            array = new int [row];
        }
    }
    //Function to input matrix
    void input()
    {
        if (type == 'l'){
            cout<<"Inputting Lower Triangular Matrix:"<<endl;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++){
                    if (i<j) sparseMatrix [i][j] = 0;
                    else{
                        cout<<"Value at ["<<i+1<<"]["<<j+1<<"] : ";
                        cin>>sparseMatrix[i][j];
                    }
                }
        }
        else if (type == 'u'){
            cout<<"Inputting Upper Triangular Matrix:"<<endl;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++){
                    if (i>j) sparseMatrix [i][j] = 0;
                    else{
                        cout<<"Value at ["<<i+1<<"]["<<j+1<<"] : ";
                        cin>>sparseMatrix[i][j];
                    }
                }
        }
        else{
            cout<<"Inputting DIAGONAL MATRIX:"<<endl;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++){
                    if (i!=j) sparseMatrix [i][j] = 0;
                    else{
                        cout<<"Value at ["<<i+1<<"]["<<j+1<<"] : ";
                        cin>>sparseMatrix[i][j];
                    }
                }
        }
    }
    //Function to convert provided matrix into One-D Array
    void matrixToArray()
    {
        if (type == 'l'){
            cout<<"\n *.Converting Lower Triangular Matrix into One-Directional Array.* \n"<<endl;
            int counter = 0;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++){
                    if (i>=j){
                        array [counter] = sparseMatrix[i][j];
                        counter++;
                    }
                }
        }
        else if (type == 'u'){
            cout<<"\n *.Converting Upper Triangular Matrix into One-Directional Array.* \n"<<endl;
            int counter = 0;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++){
                    if (i<=j){
                        array [counter] = sparseMatrix[i][j];
                        counter++;
                    }
                }
        }
        else{
            cout<<"\n *.Converting Diagonal Matrix into One-Directional Array.* \n"<<endl;
            int counter = 0;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++){
                    if (i==j){
                        array [counter] = sparseMatrix[i][j];
                        counter++;
                    }
                }
        }
    }
    //To Display Array
    void display(){
        cout<<"\n One-Dimensional Array: {";
        for (int i = 0; i< numOfElements; i++){
            cout<<array[i];
            if (i < numOfElements-1) cout<<",";
        }
        cout<<"} \n"<<endl;
    }
};

int main()
{
    system("clear");
    int choice, m, n;
    do{
        cout<<"Choose the function you wish to perform:"<<endl;
        cout<<"(1) Upper-Triangular Matrix to One-Dimensional Array"<<endl;
        cout<<"(2) Lower-Triangular/Triangular Matrix to One-Dimensional Array"<<endl;
        cout<<"(3) Diagonal Matrix to One-Dimensional Array"<<endl;
        cout<<"(0) Exit"<<endl;
        cin>>choice;
        switch(choice){
            case 1:{
                cout<<"Enter the number of Rows: ";
                cin>>m;
                cout<<"Enter the number of Columns: ";
                cin>>n;
                Matrix obj(m,n,'u');
                obj.input();
                obj.matrixToArray();
                obj.display();
            }
            break;
            case 2:{
                cout<<"Enter the number of Rows: ";
                cin>>m;
                cout<<"Enter the number of Columns: ";
                cin>>n;
                Matrix obj(m,n,'l');
                obj.input();
                obj.matrixToArray();
                obj.display();
            }
            break;
            case 3:{
                cout<<"Enter the number of Rows: ";
                cin>>m;
                cout<<"Enter the number of Columns: ";
                cin>>n;
                Matrix obj(m,n,'d');
                obj.input();
                obj.matrixToArray();
                obj.display();
            }
            break;
            case 0:{}
            break;
            default:{
            cout<<"INVALID CHOICE! Enter the correct option!"<<endl;
            }
        }
    }while(choice!=0);
    return 0;
}
