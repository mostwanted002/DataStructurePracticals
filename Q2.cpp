//Program to perform Differnet Sorting
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>


using namespace std;

template <class t> class Sorter
{
private:
    void swapNumber(t *a, t *b)
    {
        int temp = *a;
        *a = *b;
        *b = temp;
    }
    void printArray(t* array, int size)
    {
        for (int i = 0; i<size; i++)
            cout<<array[i]<<" ";
        cout<<endl;
    }
public:
    void bubbleSort(t* array, int size)
    {
        for(int i = 0; i<size-1; i++)
            for (int j = 0; j < (size - 1 - i); j++)
            if (array[j]>array[j+1])
            {
                int temp = array[j];
                array [j] = array [j+1];
                array[j+1] = temp;
                cout<<"Array after sort: ";
                printArray(array, size);
            }
    }
    void insertionSort(t* array, int size)
    {
        int key,j,i;
        for (i = 1; i < size; i++)
        {
            key = array[i];
            j = i - 1;
            while (j>=0&&array[j]>key)
            {
                array[j+1] = array[j];
                j--;
            }
            array[j+1] = key;
            cout<<"Array after sort: ";
            printArray(array, size);
        }
    }
    void selectionSort(t* array, int size)
    {
        int min_element,i,j;
        for (i = 0; i < (size-1); i++)
        {
            min_element = i;
            for(j = i +1; j < size ; j++)
                if(array[j] < array[min_element])
                    min_element = j;
            swapNumber(&array[i], &array[min_element]);
            cout<<"Array after sort: ";
            printArray(array,size);
        }
    }    
};

            
int main()
{
    int* arrayOfInt;
    int n;
    char choice;
    
    Sorter <int> obj;
    do
    {
        cout<<"Enter the size of array :";
        cin>>n;
        arrayOfInt = new int [n];
        cout<<"Enter the elements of array :";
        for(int i = 0 ; i<n; i++)
            cin>>arrayOfInt[i];
        cout<<"Choose the method of sort:- "<<endl;
        cout<<"(B)ubble Sort"<<endl;
        cout<<"(I)nsertion Sort"<<endl;
        cout<<"(S)election Sort"<<endl;
        cout<<"(E)xit"<<endl;
        cout<<"Enter the correct :";
        cin>>choice;
        switch (choice)
        {
            case 'B':
            {
                obj.bubbleSort(arrayOfInt, n);
                getchar();
            }
            break;
            case 'I':
            {
                obj.insertionSort(arrayOfInt, n);
                getchar();
            }
            break;
            case 'S':
            {
                obj.selectionSort(arrayOfInt, n);
                getchar();
            }
        }
    }while (choice != 'E');
    return 0;
}
