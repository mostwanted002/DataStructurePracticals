//Different Funtions with Recursion and Iteration.
//Program written by
//Mayank Malik
//B.Sc.(Hons.) Computer Science
//Roll No. 17013570024
//University of Delhi


#include<iostream>
#include<stdlib.h>

using namespace std;

template <class t> class dataType
{
public:
    long int factorialR(t n)
    {
        if (n == 0|| n== 1) return 1;
        else return n*factorialR(n-1);
    }
    
    long int factorialI(t n)
    {
        long int fact=1;
        for (n;n>0;n--)
            fact *= n;
        return fact;
    }
    
    int gcdR(t x, t y)
    {
        if (x<y)
        {
            if (y%x == 0) return x;
            else return (y%x, x);
        }
        else
        {
            if (x%y == 0) return y;
            else return (y,x%y);
        }
    }
    
    int gcdI(t x, t y)
    {
        if (x%y == 0 && x>y)
            return y;
        else if (y%x == 0 && y>x)
            return x;
        else
        {
            int flag = 0, gcd;
            for (int i = 1; i <= x && i <= y ; i++)
            {
                if(x%i == 0 && y%i == 0 && i!=1)
                {
                    flag++; gcd = i;
                }
            }
            if (flag > 1) return gcd;
            else return 1;
        }
    }
    
    int fibbionaciR (t n)
    {
        if (n <= 1) return n;
        else return fibbionaciR(n-1) + fibbionaciR (n-2);
    }
    int fibbionaciI (t n)
    {
        int prev = 1;
        int next = 1;
        int current = 1;
        for (int i = 3; i <= n; i++)
        {
            next = current + prev;
            prev = current;
            current = next;
        }
        return next;
    }
    
    int factorsR(t n, t i)
    {
        if (i == 0|| i == 1) return 1;
        else if (n%i==0)
        {
            cout<<i<<" ";
        }
        return factorsR(n, i-1);
    }
    
    void factorsI(t n)
    {
        for(int i = 1; i <= n ; i++)
        {
            if( (n%i) == 0)
                cout<<i<<" ";
        }
    }
};

int main()
{
    dataType <int> object;
    int choice;
    do
    {
        cout<<"(1) Factorial using RECURSION"<<endl;
        cout<<"(2) Factorial using ITERATION"<<endl;
        cout<<"(3) GCD using RECURSION"<<endl;
        cout<<"(4) GCD using ITERATION"<<endl;
        cout<<"(5) Fibbionaci using RECURSION"<<endl;
        cout<<"(6) Fibbionaci using ITERATION"<<endl;
        cout<<"(7) Factorisation using RECURSION"<<endl;
        cout<<"(8) Factorisation using ITERATION"<<endl;
        cout<<"(0) EXIT"<<endl;
        cout<<"Choose the function to execute: ";
        cin>>choice;
        switch(choice)
        {
            case 1:
            {
                int x;
                cout<<"Enter the number to calculate Factorial of: ";
                cin>>x;
                cout<<"The Factorial of given number is "<<object.factorialR(x)<<endl;
            }
            break;
            case 2:
            {
                int x;
                cout<<"Enter the number to calculate Factorial of: ";
                cin>>x;
                cout<<"The Factorial of given number is "<<object.factorialI(x)<<endl;
            }
            break;
            case 3:
            {
                int x,y;
                cout<<"Enter the numbers to calculate GCD: ";
                cin>>x>>y;
                cout<<"The GCD of given numbers is "<<object.gcdR(x,y)<<endl;
            }
            break;
            case 4:
            {
                int x,y;
                cout<<"Enter the numbers to calculate GCD: ";
                cin>>x>>y;
                cout<<"The GCD of given numbers is "<<object.gcdI(x,y)<<endl;
            }
            break;
            case 5:
            {
                int x;
                cout<<"Enter the number to display Fibbionaci of: ";
                cin>>x;
                cout<<object.fibbionaciR(x)<<endl;
            }
            break;
            case 6:
            {
                int x;
                cout<<"Enter the number to display Fibbionaci of: ";
                cin>>x;
                cout<<object.fibbionaciI(x)<<endl;
            }
            break;
            case 7:
            {
                int x;
                cout<<"Enter the number to Factorise: ";
                cin>>x;
                cout<<object.factorsR(x,x)<<endl;
            }
            break;
            case 8:
            {
                int x;
                cout<<"Enter the number to Factorise: ";
                cin>>x;
                cout<<"The Factors of given number are: ";object.factorsI(x);
                cout<<endl;
            }
            break;
        }
    }while (choice != 0);
    return 0;
}
